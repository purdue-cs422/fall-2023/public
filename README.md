<img src="others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# CS 422: Computer Networks (Fall 2023)  

[[_TOC_]]

## Logistics 
 
- Instructor: [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
- Teaching assistants: 
  - [Rakin Haider](https://sites.google.com/site/rakinhaider/)
  - [Milad Najafabadi](https://www.cs.purdue.edu/people/graduate-students/mesrafil.html)
- Lecture time: **TTh 6:30-7:45pm**
- Location: LWSN B155	
- Credit Hours: 3.00
- Course discussion and announcements: [Campuswire](https://campuswire.com/c/G61521943)
- Paper reviews: [Perusall](https://app.perusall.com/courses/fall-2023-cs-42200-le1-lec/_/dashboard)
- Development environment: [AWS Academy](https://awsacademy.instructure.com/courses/55492)
- Exam submission: [Gradescope](https://www.gradescope.com/courses/571171)
- Office hours
  - Wednesday 3:00-4:00pm, [Zoom](https://purdue-edu.zoom.us/j/93810395451?pwd=RExkeXE5T2IwbUVkOWMyeThtQUdBdz09), Muhammad Shahbaz
  - Thursday 3:00-4:00pm, [Zoom](https://purdue-edu.zoom.us/j/97353408358) Rakin Haider
  - Tuesdays 5:30-6:30pm, [Zoom](https://purdue-edu.zoom.us/j/3088367935) Milad Najafabadi
- Practice study observation (PSO), HAAS G50
  - Wednesday 1:30-2:20pm, Milad Najafabadi
  - Thursday 11:30am-12:20pm, Rakin Haider

> **Note:** Visit [Brightspace](https://purdue.brightspace.com/d2l/home/832201) for instructions on joining [Campuswire](https://campuswire.com/c/G61521943), [Gradescope](https://www.gradescope.com/courses/571171), [Perusall](https://app.perusall.com/courses/fall-2023-cs-42200-le1-lec/_/dashboard), and [AWS Academy](https://awsacademy.instructure.com/courses/55492).

#### Suggesting edits to the course page and more ...

We strongly welcome any changes, updates, or corrections to the course page or assignments or else that you may have. Please submit them using the [GitLab merge request workflow](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).

## Course Description

CS 422 is an undergraduate-level course in Computer Networks at Purdue University. In this course, we will explore the underlying principles and design decisions that have enabled the Internet to (inter)connect billions of people and trillions of things on and off this planet---especially under the post-COVID-19 era. We will study the pros and cons of the current Internet design, ranging from classical problems (e.g., packet switching, routing, naming, transport, and congestion control) to emerging and future trends, like data centers, software-defined networking (SDN), programmable data planes, and network function virtualization (NFV) to name a few.

The goals for this course are:

- To become familiar with the classical and emerging problems in networking and their solutions.
- To learn what's the state-of-the-art in networking research: network architecture, protocols, and systems.
- To gain some practice in reading research papers and critically understanding others' research.
- To gain experience with network programming using industry-standard and state-of-the-art networking platforms.

# Course Syllabus and Schedule

> **Notes:** 
> - This syllabus and schedule is preliminary and subject to change.
> - Everything is due at 11:59 PM (Eastern) on the given day.
> - Abbreviations refer to the following:
>   - PD: Peterson/Davie (online version)
>   - KR: Kurose/Ross (6th edition)
>   - SDN: Peterson/Cascone/O’Connor/Vachuska/Davie (online version)
>   - 5G: Peterson/Sunay (online version)
>   - GV: George Varghese (1st edition)

| Date    | Topics  | Notes | Readings |
| :------ | :------ | :------  | :------ |
| **Week 1** | **Course Overview** | | |
| Tue <br> Aug 22 | Introduction ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14068014/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7289608&srcou=832201)) | | |
| Thu <br> Aug 24 | A Brief History of the Internet ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14092520/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7289609&srcou=832201)) | | &bull; [How to Read a Paper](https://gitlab.com/purdue-cs422/fall-2023/public/-/raw/main/readings/HowToRead2017.pdf) |
| **Week 2** | **Network Building Blocks** | | |
| Tue <br> Aug 29 | Layering and Protocols ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14107642/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7303856&srcou=832201)) | | &bull; PD: [1.3 (Architecture)](https://book.systemsapproach.org/foundation/architecture.html) <br> &bull; [End-to-End Arguments](https://gitlab.com/purdue-cs422/spring-2022/public/-/raw/main/readings/e2eArgument84.pdf) (Optional) |
| Thu <br> Aug 31 | [AWS Academy](https://gitlab.com/purdue-cs422/fall-2023/public/-/blob/main/assignments/cs422-awsacademy-hotwo.pdf) and [Assignment 0](assignments/assignment0) Walkthrough | Instructors: [Rakin Haider](https://sites.google.com/site/rakinhaider/) and [Milad Najafabadi](https://www.cs.purdue.edu/people/graduate-students/mesrafil.html) | |
| **Week 3** | **The Network API** | | |
| Tue <br> Sep 05 | Sockets: The Network Interface ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14138267/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7332585&srcou=832201)) | &bull; [Demo](demos/Sockets) | &bull; PD: [1.4 (Software)](https://book.systemsapproach.org/foundation/software.html) <br> &bull; [Beej's Guide](http://beej.us/guide/bgnet/) (Optional) |
| Thu <br> Sep 07 | *Presidential Lecture Series: [Vint Cerf and Robert Kahn](https://www.purdue.edu/president/lecture-series/a-conversation-with-internet-founders-vint-cerf-and-robert-kahn-and-purdue-president-mung-chiang/)!* :tada: <br> No Class  | | |
| **Week 4** | **Local Area Networks I** | | |
| Tue <br> Sep 12 | Direct Links: The Wire Interface ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14166611/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7363244&srcou=832201)) | &bull; [Paper Review 1](https://app.perusall.com/courses/fall-2023-cs-42200-le1-lec/a-protocol-for-packet-network-intercommunication?assignmentId=X9HXuurwX7nn3xMv6&part=1) `due Oct 16` <br> &bull; [Assignment 1](assignments/assignment1) `due Sep 25` | &bull; PD: [2.1 - 2.6 (Technology, Encoding, Framing, ...)](https://book.systemsapproach.org/direct.html) |
| Thu <br> Sep 14 | Indirect Links: Internetworking (L2/L3) ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14176629/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7374418&srcou=832201)) | &bull; [Quiz 1](https://www.gradescope.com/courses/571171/assignments/3329769/submissions) `due Sep 15`  | &bull; PD: [3.2 - 3.3 (Switched Ethernet, Internet)](https://book.systemsapproach.org/internetworking.html) |
| **Week 5** | **Local Area Networks II** | | |
| Tue <br> Sep 19 | Indirect Links: Internetworking Protocols ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14202451/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7398670&srcou=832201)) | &bull; [Demo](demos/ARP) | &bull; PD: [3.3.1, 3.3.2, 3.3.6 (Inernetwork, Service Model, ARP)](https://book.systemsapproach.org/internetworking.html) |
| Thu <br> Sep 21 | Guest Lecture by [Enkeleda Bardhi](https://bardhienkeleda.github.io/#intro): Network Security 101! ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14211640/View)) :tada: | | |
| **Week 6** | **Network Addressing and Configuration** | | |
| Tue <br> Sep 26 | Flat/Classful Addressing: Ethernet, IP, and Subnets ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14229625/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7425011&srcou=832201)) | | &bull; PD: [3.3.3 - 3.3.9 (Addressing, DHCP ...)](https://book.systemsapproach.org/internetworking.html) |
| Thu <br> Sep 28 | Classless Addressing: CIDR ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14242186/View), video) | | &bull; PD: [3.3.5 (Classless Addressing)](https://book.systemsapproach.org/internetworking.html) |
| **Week 7** | **Process-to-Process Communication I** | | |
| Tue <br> Oct 03 | Transport: Reliable Delivery ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14253354/View), video) | &bull; [Assignment 2](assignments/assignment2) `due Oct 16` | &bull; PD: [2.5 (Reliable Transmission)](https://book.systemsapproach.org/direct/reliable.html) <br> &bull; PD: [5.1 - 5.2 (UDP, TCP)](https://book.systemsapproach.org/e2e.html) |
| Thu <br> Oct 05 | [Assignment 2](assignments/assignment2) Powwow | | |
| Fri <br> Oct 06 | | &bull; [Quiz 2](https://www.gradescope.com/courses/571171/assignments/3481716/submissions) `due Oct 09` | |
| **Week 8** | **Process-to-Process Communication II** | | |
| Tue <br> Oct 10 | *October Break* <br> No Class | | |
| Thu <br> Oct 12 | Transport: Flow Control ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14699059/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7481107&srcou=832201)) | | &bull; PD: [5.2 (TCP)](https://book.systemsapproach.org/e2e.html) <br> &bull; PD: [5.3 - 5.4 (RPC, RTP)](https://book.systemsapproach.org/e2e.html) (Optional) |
| **Week 9** | **Midterm Week** | | |
| Tue <br> Oct 17 | *Midterm Prep* | | |
| Thu <br> Oct 19 | *Midterm Exam* | | |
| **Week 10** | **Software-Defined Networks and Data Centers** | | |
| Tue <br> Oct 24  | Control-Plane Abstractions and OpenFlow ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14727705/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7507650&srcou=832201)) | | &bull; How SDN will Shape Networking ([video](https://www.youtube.com/watch?v=c9-K5O_qYgA)) <br> &bull; SDN: [3: Basic Architecture](https://sdn.systemsapproach.org/arch.html) |
| Thu <br> Oct 26 | Origin and Architectures of Data Centers ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14727708/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7507652&srcou=832201)) | | &bull; [MapReduce](https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf) and an article on Jeff Dean and Sanjay Ghemawat on why it came into being ([link](https://www.newyorker.com/magazine/2018/12/10/the-friendship-that-made-google-huge)) |
| **Week 11** | **Wide Area Networks I** | | |
| Tue <br> Oct 31 | Direct Networks: Intradomain Routing ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14784964/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7559054&srcou=832201)) | &bull; [Assignment 3](assignments/assignment3) `due Nov 17` | &bull; PD: [3.4.2 - 3.4.3 (Routing: Distance Vector and Link State)](https://book.systemsapproach.org/internetworking/routing.html) |
| Thu <br> Nov 02 | Indirect Networks: Interdomain Routing ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14787415/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7561838&srcou=832201)) | &bull; [Paper Review 2](https://app.perusall.com/courses/fall-2023-cs-42200-le1-lec/the-road-to-sdn-an-intellectual-history-of-programmable-networks?assignmentId=rqqvpKxfojJcQgzy8&part=1) `due Dec 01` | &bull; PD: [4.1 (Global Internet)](https://book.systemsapproach.org/scaling/global.html) |
| **Week 12** | **Programmable Networks (and Network Data Planes)** | | |
| Tue <br> Nov 07 | Protocol-Independent Switching: Bottoms Up vs Top Down ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14811629/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7584086&srcou=832201)) | | &bull; SDN: [4 (Bare-Metal Switches)](https://sdn.systemsapproach.org/switch.html) |
| Thu <br> Nov 09 | Router Design: Lookup and Scheduling ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14845278/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7611866&srcou=832201)) | | &bull; KR: 4.3 (What's Inside a Router?) <br> &bull; GV: [10 (Exact-Match Lookups), 11 (Prefix-Match Lookups), 13 (Switching)](https://purdue.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma99169139049601081&context=L&vid=01PURDUE_PUWL:PURDUE&lang=en&search_scope=MyInst_and_CI&adaptor=Local%20Search%20Engine&tab=Everything&query=any,contains,george%20varghese) |
| **Week 13** | **Wide Area Networks II** | | |
| Tue <br> Nov 14 | *Guest Lecture by [Bruce Davie](https://www.linkedin.com/in/bruce-davie/)!* ([video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7611869&srcou=832201)) :tada: | | |
| Thu <br> Nov 16 | Indirect Networks: Peering and IXPs ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14851327/View), video) | &bull; [Quiz 3](https://www.gradescope.com/courses/571171/assignments/3691734/submissions) `due Nov 19` | &bull; KR: 4.6.3 (Inter-AS Routing: BGP) <br> &bull; [Where is Internet Congestion Occuring?](https://medium.com/network-insights/where-is-internet-congestion-occurring-e4333ed71168) |
| **Week 14** | **Network Applications** | | |
| Tue <br> Nov 21 | The World Wide Web (HTTP) ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14851328/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7615942&srcou=832201)) | | &bull; PD: [9.1.2 (World Wide Web)](https://book.systemsapproach.org/applications/traditional.html#world-wide-web-http) |
| Thu <br> Nov 23 | *Thanksgiving Holiday* <br> No Class | | |
| **Week 15** | **Resource Allocation** | | |
| Mon <br> Nov 27 | | &bull; [Assignment 4](assignments/assignment4) `due Dec 03` (Optional) | |
| Tue <br> Nov 28 | Congestion Control and Queuing Disciplines ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14911499/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7664394&srcou=832201)) | | &bull; PD: [6.1 - 6.2 (Issues, Queuing)](https://book.systemsapproach.org/congestion.html) |
| Thu <br> Nov 30 | Transport: Congestion Control (TCP) ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14911500/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=832201&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-7664395&srcou=832201)) | | &bull; PD: [6.3 - 6.4 (TCP, ...)](https://book.systemsapproach.org/congestion.html) |
| Fri <br> Dec 01 | | &bull; [Quiz 4](https://www.gradescope.com/courses/571171/assignments/3750356/submissions) `due Dec 03` | |
| **Week 16** | **Parting Thoughts & Course Summary** | | |
| Tue <br> Dec 05 | Research Papers Discussion ([ppt](https://purdue.brightspace.com/d2l/le/content/832201/viewContent/14935279/View), video) | | |
| Thu <br> Dec 07 | *Final Review* | | |
| **Week 17** | **Exam Week** | | |
| Tue <br> Dec 12 | | | |
| Thu <br> Dec 14 | *Final Exam* | &bull; Time and place: **7:00pm in WALC 2007** | |

## Prerequisites

This course assumes that students have a basic understanding of data structures and algorithms and experience with programming languages like C/C++ and Python. Please see [CS 240](https://selfservice.mypurdue.purdue.edu/prod/bwckctlg.p_disp_course_detail?cat_term_in=202120&subj_code_in=CS&crse_numb_in=24000), [CS 380](https://selfservice.mypurdue.purdue.edu/prod/bwckctlg.p_disp_course_detail?cat_term_in=202120&subj_code_in=CS&crse_numb_in=38003), or similar courses at Purdue for reference.

## Recommended Textbooks
- Computer Networking: A Top-Down Approach by J. Kurose and K. Ross (6th Edition)
- Computer Networks: A Systems Approach by L. Peterson and B. Davie ([Online Version](https://book.systemsapproach.org/index.html))
- Software-Defined Networks: A Systems Approach by L. Peterson, C. Cascone, B. O’Connor, T. Vachuska, and Bruce Davie ([Online Version](https://sdn.systemsapproach.org/index.html))
- 5G Mobile Networks: A Systems Approach by L. Peterson and O. Sunay ([Online Version](https://5g.systemsapproach.org/index.html))

> Other optional but interesting resources: [Sytems Approach - Blog](https://www.systemsapproach.org/blog), [TCP Congestion Control: A Systems Approach](https://tcpcc.systemsapproach.org/index.html), [Operating an Edge Cloud: A Systems Approach](https://ops.systemsapproach.org), and [Network Algorithmics: An Interdisciplinary Approach to Designing Fast Networked Devices](https://purdue.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma99169139049601081&context=L&vid=01PURDUE_PUWL:PURDUE&lang=en&search_scope=MyInst_and_CI&adaptor=Local%20Search%20Engine&tab=Everything&query=any,contains,george%20varghese)

## Paper Reading and Discussion

During the course, we will be reading and discussing **two** research papers on topics ranging from network protocols, systems, and architectures. You should closely read each paper and add comments and questions along with a summary (5 lines or so) of the paper on [Perusall](https://app.perusall.com/courses/fall-2023-cs-42200-le1-lec/_/dashboard) by the due date below. Please plan to provide at least five comments or questions for each paper on Perusall and follow the comments from other students and the course staff. 

> **Note:** General tips on reading and reviewing papers are [here](reading-papers.md). 

Grades for your class participation and paper reviews will be determined based on attendance and, more importantly, contributions to paper discussions on Perusall.

### Reading list

- [Paper 1: A Protocol for Packet Network Intercommunication](https://app.perusall.com/courses/fall-2023-cs-42200-le1-lec/a-protocol-for-packet-network-intercommunication?assignmentId=X9HXuurwX7nn3xMv6&part=1) `due Oct 16`
- [Paper 2: The Road to SDN: An Intellectual History of Programmable Networks](https://app.perusall.com/courses/fall-2023-cs-42200-le1-lec/the-road-to-sdn-an-intellectual-history-of-programmable-networks?assignmentId=rqqvpKxfojJcQgzy8&part=1) `due Dec 01`

## Programming Assignments

- [Assignment 0: Virtual Networks using Mininet and ONOS](assignments/assignment0) `not graded`
- [Assignment 1: File and Message Transmission using Sockets](assignments/assignment1) `due Sep 25`
- [Assignment 2: From Bridging to Switching with VLANs](assignments/assignment2) `due Oct 16`
- [Assignment 3: DNS Reflection Attacks' Detection and Mitigation](assignments/assignment3) `due Nov 17`
- [Assignment 4: From Bridging to Switching using P4's Match-Action Tables (MATs)](assignments/assignment4) `due Dec 03` (Optional)

> **A ChatGPT-based project completed by one of the students: [`Medium Post`](https://medium.com/@miller.oliver/bcf63d251247).**

## Quizzes

- [Quiz 1: Topics from weeks 1-3](https://www.gradescope.com/courses/571171/assignments/3329769/submissions) `due Sep 15`
- [Quiz 2: Topics from weeks 4-6](https://www.gradescope.com/courses/571171/assignments/3481716/submissions) `due Oct 09`
- [Quiz 3: Topics from weeks 10-12](https://www.gradescope.com/courses/571171/assignments/3691734/submissions) `due Nov 19`
- [Quiz 4: Topics from weeks 13-15](https://www.gradescope.com/courses/571171/assignments/3750356/submissions) `due Dec 03`

> **Format:** take home, open book

## Midterm and Final Exams
There will be one midterm and a final exam based on course content (lectures, assignments, and paper readings).

- Midterm Exam `on Oct 19` 
- Final Exam `on Dec 14 @ 7:00pm in WALC 2007`

> **Format:** in class, open book

## Grading

- Class participation and discussions: 10%
- Programming assignments: 40%
- Quizzes: 15%
- Midterm exam: 15%
- Final exam: 20%

## Policies

### Late submission

- Grace period: 24 hours for the entire semester.
- After the grace period, 25% off for every 24 hours late, rounded up.

If you have extenuating circumstances that result in an assignment being late, please let us know about them as soon as possible.

### Academic integrity

We will default to Purdue's academic policies throughout this course unless stated otherwise. You are responsible for reading the pages linked below and will be held accountable for their contents.
- http://spaf.cerias.purdue.edu/integrity.html
- http://spaf.cerias.purdue.edu/cpolicy.html

### Honor code

By taking this course, you agree to take the [Purdue Honors Pledge](https://www.purdue.edu/odos/osrr/honor-pledge/about.html): "As a boilermaker pursuing academic excellence, I pledge to be honest and true in all that I do. Accountable together - we are Purdue."

## Acknowledgements

This class borrows inspirations from several incredible sources.

- The course syllabus page format loosely follows Xin Jin's [EN.601.414/614](https://github.com/xinjin/course-net) class at John Hopkins.
- The lecture slides' material is partially adapted from my Ph.D. advisors, Jen Rexford's [COS 461](https://www.cs.princeton.edu/courses/archive/fall20/cos461) class and Nick Feamster's [COS 461](https://www.cs.princeton.edu/courses/archive/spring19/cos461/) class at Princeton.
